(defproject ladders "0.1.0-SNAPSHOT"
  :description "Initrode's proprietary definetely not spam emailer."
  :url "http://emailer.initrode.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha15"]
                 [org.clojure/test.check "0.9.0"]]
  :main ^:skip-aot ladders.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
