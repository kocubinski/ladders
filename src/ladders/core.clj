(ns ladders.core
  (:require [ladders.gen :as gen]
            [clojure.core.reducers :as r]
            [clojure.set :as set]))

(def ^:const spam-ceiling 0.05)

(def ^:const spam-window-ceiling 0.1)

(def ^:const spam-score-ceiling 0.3)

(def ^:const window-size 100)

(defn send-email [email]
  ;; I don't do anything...
  ;; Definetely not spamming for herbal supplements
  )

(defn validate-email [{:keys [sent sum]} {:keys [email-address spam-score]}]
  (cond
    (sent email-address) ::already-sent
    (> (/ (+ sum spam-score) (inc (count sent))) spam-ceiling) ::above-mean
    :else ::ok))

(defn combine-emails [a b]
  {:sent (set/union (:sent a) (:sent b))
   :sum (+ (:sum a) (:sum b))
   :deferred (concat (:deferred a) (:deferred b))})

(defn email-combiner
  ([] {:sent #{} :sum 0 :deferred []})
  ([a b] (combine-emails a b)))

(defn email-reducer [{:keys [sent sum deferred] :as acc}
                     {:keys [email-address spam-score] :as email-record}]
  (condp = (validate-email acc email-record)
    ::already-sent acc
    ::above-mean (update acc :deferred conj email-record)
    ::ok (assoc acc :sent (conj sent email-address) :sum (+ sum spam-score))))

(defn process-emails [emails]
  (->> emails
       (filter #(<= (:spam-score %) spam-score-ceiling))
       (reduce email-reducer {:sent #{} :sum 0 :deferred (list)})
       ((fn [{:keys [deferred] :as acc}]
          (println "deferred" (count deferred))
          (reduce email-reducer
                  (assoc acc :deferred (list))
                  (sort-by :spam-score deferred))))
       ((fn [{:keys [sent sum deferred]}]
          (println "deferred" (count deferred))
          {:count (count sent) :sum sum :mean (/ sum (count sent))}))))

;; parallelization

(defn distinct-by
  [f xs]
  (let [s (volatile! #{})]
    (for [x xs
          :let [id (f x)]
          :when (not (contains? @s id))]
      (do (vswap! s conj id)
          x))))

(defn slow-distinct-combiner
  ([] {:seen #{} :emails []})
  ([a b]
   (let [dupes (set/intersection (:seen a) (:seen b))]
     {:seen (set/union (:seen a) (:seen b))
      :emails (concat (:emails a) (filter #(not (dupes %)) (:emails b)))})))

(defn slow-distinct-reducer [{:keys [seen emails] :as acc}
                             {:keys [email-address] :as record}]
  (if (seen email-address) acc
      (-> acc
          (update :seen conj email-address)
          (update :emails conj record))))

(defn distinct-filter [coll]
  (let [seen (atom #{})]
    (r/filter (fn [{:keys [email-address]}]
                (when-not (contains? @seen email-address)
                  (swap! seen conj email-address)))
              coll)))

(defn pemail-combiner
  ([] {:count 0 :sum 0 :deferred []})
  ([a b]
   {:count (+ (:count a) (:count b))
    :sum (+ (:sum a) (:sum b))
    :deferred (concat (:deferred a) (:deferred b))}))

(defn pemail-reducer [{:keys [count sum deferred] :as acc}
                     {:keys [email-address spam-score] :as email-record}]
  (if (> (/ (+ sum spam-score) (inc count)) spam-ceiling)
    (update acc :deferred conj email-record)
    (-> acc
        (update :count inc)
        (update :sum + spam-score))))

(defn parallelized-process-emails [emails]
  (->> emails

       ;; serial distinct
       ;;(distinct-by :email-address)
       ;;vec

       (distinct-filter)

       (r/filter #(<= (:spam-score %) spam-score-ceiling))
       (r/fold pemail-combiner pemail-reducer)

       ((fn [{:keys [deferred] :as acc}]
          (println "deferred" (count deferred))
          (reduce pemail-reducer
                  (assoc acc :deferred (list))
                  (sort-by :spam-score deferred))))
       
       ((fn [{:keys [count sum deferred] :as acc}]
          (println "deferred" (clojure.core/count deferred))
          (-> acc
              (dissoc :deferred)
              (assoc :mean (/ sum count)))
       ))))

;; just curious
(defn mean-spam-score []
  (/ (->> (gen/repeat-emails 10000)
          (map :spam-score)
          (reduce +))
     10000))

(defn spam-valued-customers [n]
  (time (println (process-emails (gen/repeat-emails n))))
  (time (parallelized-process-emails (gen/repeat-emails n)))
  )
