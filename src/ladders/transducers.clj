(ns ladders.transducers)

(defn map-reducer [f]
  (fn [result input]
    (conj result (f input))))

(reduce (map-reducer inc) [] (range 10))

(reduce (map-reducer #(* % %)) [] (range 10))

;; etc

(defn filter-reducer [predicate]
  (fn [result input]
    (if (predicate input)
      (conj result input)
      result)))

(reduce (filter-reducer even?) [] (range 10))

(->> (range 10)
     (reduce (filter-reducer even?) [])
     (reduce (map-reducer dec) []))

;; ==

(->> (range 10)
     (filter even?)
     (map dec))

;; transducer-like

(defn mapping [f]
  (fn [reducing]
    (fn [result input]
      (reducing result (f input)))))

(defn filtering [predicate]
  (fn [reducing]
    (fn [result input]
      (if (predicate input)
        (reducing result input)
        result))))

(->> (range 10)
     (reduce ((mapping inc) conj) [])
     (reduce ((filtering even?) conj) []))

;; better


(((mapping inc) conj) [] 1)

(((mapping identity) conj) [] 1)

(def xform
  (comp
   (mapping inc)
   (filtering even?)))

(reduce (xform conj) [] (range 10))
